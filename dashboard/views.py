# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from messagerie.models import Message

@login_required
def index(request):
    users = User.objects.all()
    me = request.user
    
    ask = []
    count_ask = me.membre.demandes.count()
    if me.membre.demandes.count() != 0:
        ask = me.membre.demandes.all()
        
    friend = []
    if me.membre.amis.count() != 0:
        friend = me.membre.amis.all()
    
    attente = []
    if me.membre.attentes.count() != 0:
        attente = me.membre.attentes.all()
        
    message = []
    if Message.objects.all().filter(destinataire=me.username).count() != 0:
        message = Message.objects.all().filter(destinataire=me.username)
    
    if request.POST:
        if "envoyer_demande" in request.POST:
            for demande in request.POST.getlist('demande'):
                people = User.objects.get(username=demande)
                people.membre.demandes.add(me.membre)
                me.membre.attentes.add(people.membre)
        if "accepter_demande" in request.POST:
            for who in request.POST.getlist('who'):
                people = User.objects.get(username=who)
                me.membre.demandes.remove(people.membre)
                me.membre.amis.add(people.membre)
                people.membre.attentes.remove(me.membre)
        if "refuser_demande" in request.POST:
            for who in request.POST.getlist('who'):
                people = User.objects.get(username=who)
                me.membre.demandes.remove(people.membre)
                people.membre.attentes.remove(me.membre)
        if "envoyer_message" in request.POST:
            destinataire = request.POST['destinataire']
            destinataire = User.objects.get(username=destinataire)
            
            message_objet = Message()
            message_objet.emetteur = me.membre
            message_objet.destinataire = destinataire.membre
            message_objet.duree = int(request.POST['duree'])
            message_objet.message = request.POST['message']
            message_objet.pj = request.FILES['pj']
            message_objet.save()
    
    context = {
        'user': users,
        'me': me,
        'ask': ask,
        'friend': friend,
        'attente': attente,
        'compteur': count_ask,
        'message': message
    }
        
    return render(request, "examples/dashboard/index.html", context)