from django.db import models
from django.contrib.auth.models import User

# Create your models here. 
class Membre(models.Model):
    user = models.OneToOneField(User)
    amis = models.ManyToManyField("self", related_name="amis", null=True, blank=True)
    demandes = models.ManyToManyField("self", related_name="demandes_set", null=True, blank=True)
    attentes = models.ManyToManyField("self", related_name="attentes_set", null=True, blank=True)
    
    def __unicode__(self):
        return self.user.username
        