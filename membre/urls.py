from django.conf.urls import patterns, url
from django.contrib.auth.views import logout


urlpatterns = patterns('membre.views',
                       url(r'^se-deconnecter/$', logout, {'next_page': "/"}, name="logout"),
                    )
