from django.contrib import admin
from membre.models import Membre

class MembreAdmin(admin.ModelAdmin):
    pass

admin.site.register(Membre,MembreAdmin)