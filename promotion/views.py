from django.shortcuts import render
from django.contrib.auth import login as auth_login, authenticate
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from membre.models import Membre
from membre.forms import AuthenticationForm
from membre.forms import RegistrationForm


def index(request):

    messaging_url = "/dashboard/index/" # Are we going to a static template ?

    authentication_form = AuthenticationForm(request, prefix='authentication')
    registration_form = RegistrationForm(prefix='registration')
    
    if request.user in User.objects.all():
        return HttpResponseRedirect(messaging_url)
    
    if request.method == "POST":
        user = None
        if "login" in request.POST:
            authentication_form = AuthenticationForm(request, data=request.POST, prefix='authentication')
            if authentication_form.is_valid():
                # Okay, security check complete. Get the user.
                user = authentication_form.get_user()
        elif "register" in request.POST:
            registration_form = RegistrationForm(data=request.POST, prefix='registration')
            if registration_form.is_valid():
                # Register the new user !
                name = registration_form['username'].value()
                pw = registration_form['password'].value()
                email = registration_form['email'].value()
                exist = False
                for people in User.objects.all():
                    if name == people.username:
                        exist = True
                if exist==False:
                    my_user = User.objects.create_user(name, email, pw)       
                    m = Membre()
                    m.user = my_user
                    m.save()
                    login_user = authenticate(username=name, password=pw)
                    auth_login(request, login_user) 
                    return HttpResponseRedirect(messaging_url)
                
        if user is not None:
            auth_login(request, authentication_form.get_user())
            return HttpResponseRedirect(messaging_url)

    context = {
        'authentication_form': authentication_form,
        'registration_form': registration_form
    }

    return render(request, "promotion/index.html", context)

def help(request):

    return render(request, "promotion/help.html")
