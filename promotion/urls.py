from django.conf.urls import patterns, url

urlpatterns = patterns('promotion.views',
                       url('^$', 'index', name="index"),
                       url('^help.html/$', 'help', name="help"))
