from django.db import models
from django.contrib.auth.models import User
from membre.models import Membre

# Create your models here.
class Message(models.Model):
    emetteur = models.ForeignKey(Membre, blank=False, null=False, related_name="emetteur") 
    destinataire = models.ForeignKey(Membre, blank=False, null=False, related_name="destinaire")
    date = models.DateTimeField(auto_now=True)
    message = models.TextField()
    etat = models.BooleanField(default=False)
    pj = models.FileField(null=True, blank=True)
    duree = models.PositiveSmallIntegerField(default=3, choices=((1,'1 sec'), (3,'3 sec'), (10,'10 sec')))
                                             